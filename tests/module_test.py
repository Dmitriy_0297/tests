import unittest
from termgraph import termgraph as termgr
from unittest.mock import patch
from io import StringIO


class ModuleTest(unittest.TestCase):
    def test_init_args(self):
        termgr.init_args()

    def test_main(self):
        pass

    def test_find_min(self):
        value_integer = 1
        value_float = 0.1

        # Integer
        list_values_integer = [[1], [5], [10], [100], [2], [7], [90], [4]]
        min_integer = termgr.find_min(list_values_integer)

        # Float
        list_values_float = [[0.1], [3.56], [10.21], [10.3]]
        min_float = termgr.find_min(list_values_float)

        assert min_integer == value_integer and min_float == value_float

    def test_find_max(self):
        value_integer = 110
        value_float = 7.6

        # Integer
        list_values_integer = [[5], [10], [110], [100], [75], [7], [90], [4]]
        max_integer = termgr.find_max(list_values_integer)

        # Float
        list_value_float = [[0.3], [0.5], [7.6], [4.5], [3.2]]
        max_float = termgr.find_max(list_value_float)

        assert max_integer == value_integer and max_float == value_float

    def test_find_max_len(self):
        # 3 element int list_value
        max_len_value = 13

        # Use random data
        list_value = ['11121', 'ttthhtes', 'hfhrjfktiemcw', '1']

        max_len = termgr.find_max_label_length(list_value)

        assert max_len == max_len_value

    def test_normalize(self):
        # Use default value
        width = 50
        # Use data from ex6.dat category 1
        data = [[183.32], [231.23], [16.43], [50.21], [508.97], [212.05], [30.0]]
        check_list = [[18.00891997563707],
                      [22.715484213214925],
                      [1.6140440497475292],
                      [4.932510757019078],
                      [50.0],
                      [20.83128671630941],
                      [2.9471285144507533]]

        result = termgr.normalize(data, width)

        assert result == check_list


    def test_horiz_rows(self):
        # Use data from ex6.dat
        labels = ['2007', '2008', '2009', '2010', '2011', '2012', '2014']
        # Use data from ex6.dat category 1
        data = [[183.32], [231.23], [16.43], [50.21], [508.97], [212.05], [30.0]]

        # positional arguments:
        #   filename              data file name (comma or space separated). Defaults to
        #                         stdin.
        #
        # optional arguments:
        #   -h, --help            show this help message and exit
        #   --title TITLE         Title of graph
        #   --width WIDTH         width of graph in characters default:50
        #   --format FORMAT       format specifier to use.
        #   --suffix SUFFIX       string to add as a suffix to all data points.
        #   --no-labels           Do not print the label column
        #   --color [{red,blue,green,magenta,yellow,black,cyan} [{red,blue,green,magenta,yellow,black,cyan} ...]]
        #                         Graph bar color( s )
        #   --vertical            Vertical graph
        #   --stacked             Stacked bar graph
        #   --different-scale     Categories have different scales.
        #   --calendar            Calendar Heatmap chart
        #   --start-dt START_DT   Start date for Calendar chart
        #   --custom-tick CUSTOM_TICK
        #                         Custom tick mark, emoji approved
        #   --delim DELIM         Custom delimiter, default , or space
        #   --verbose             Verbose output, helpful for debugging
        args = {'filename': 'data/ex6.dat',
                'title': "Stacked Data",
                'width': 50,
                'format': '{:<5.2f}',
                'suffix': '',
                'no_labels': False,
                'color': '{yellow}',
                'vertical': True,
                'stacked': False,
                'different_scale': False,
                'calendar': False,
                'start_dt': None,
                'custom_tick': '',
                'delim': '',
                'verbose': False,
                'version': False}

        # Use data from ex6.dat category 1
        normal_data = [[18.00891997563707],
                       [22.715484213214925],
                       [1.6140440497475292],
                       [4.932510757019078],
                       [50.0],
                       [20.83128671630941],
                       [2.9471285144507533]]

        # for dataset use ex6.dat category 1
        example_list_rows = [(183.32, 18, 16.43, 93),
                             (231.23, 22, 16.43, 93),
                             (16.43, 1, 16.43, 93),
                             (50.21, 4, 16.43, 93),
                             (508.97, 50, 16.43, 93),
                             (212.05, 20, 16.43, 93),
                             (30.0, 2, 16.43, 93)]
        # Value for color:
        # 'red': 91,
        # 'blue': 94,
        # 'green': 92,
        # 'magenta': 95,
        # 'yellow': 93,
        # 'black': 90,
        # 'cyan': 96
        colors = [93]
        result_list_rows = []
        for row in termgr.horiz_rows(labels, data, normal_data, args, colors):
            result_list_rows.append(row)

        assert example_list_rows == result_list_rows

    # Use data from ex6.dat
    # Use 4 TICKS
    def test_print_row_tick(self):
        with patch('sys.stdout', new=StringIO()) as result:
            termgr.print_row('2007', 4, 10.0, None)
            output = result.getvalue().strip()
            print(output)
            # TICKS
            assert output == '▇▇▇▇'

    def test_stacked_graph(self):
        with patch('sys.stdout', new=StringIO()) as output:
            # Use data from ex6.dat
            labels = ['2007', '2008', '2009', '2010', '2011', '2012', '2014']

            # Use data from ex6.dat
            data = [[183.32, 190.52],
                    [231.23, 5.0],
                    [16.43, 53.1],
                    [50.21, 7],
                    [508.97],
                    [212.05, 20.2],
                    [10.0, 9]]

            # Use data from ex6.dat
            normal_data = [[18.00891997563707, 18.716230819105252],
                           [22.715484213214925, 0.49118808574179224],
                           [1.6140440497475292, 5.2164174705778334],
                           [4.932510757019078, 0.6876633200385092],
                           [50.0],
                           [20.83128671630941, 1.9843998663968405],
                           [0.9823761714835845, 0.884138554335226]]

            # positional arguments:
            #   filename              data file name (comma or space separated). Defaults to
            #                         stdin.
            #
            # optional arguments:
            #   -h, --help            show this help message and exit
            #   --title TITLE         Title of graph
            #   --width WIDTH         width of graph in characters default:50
            #   --format FORMAT       format specifier to use.
            #   --suffix SUFFIX       string to add as a suffix to all data points.
            #   --no-labels           Do not print the label column
            #   --color [{red,blue,green,magenta,yellow,black,cyan} [{red,blue,green,magenta,yellow,black,cyan} ...]]
            #                         Graph bar color( s )
            #   --vertical            Vertical graph
            #   --stacked             Stacked bar graph
            #   --different-scale     Categories have different scales.
            #   --calendar            Calendar Heatmap chart
            #   --start-dt START_DT   Start date for Calendar chart
            #   --custom-tick CUSTOM_TICK
            #                         Custom tick mark, emoji approved
            #   --delim DELIM         Custom delimiter, default , or space
            #   --verbose             Verbose output, helpful for debugging
            args = {'filename': 'data/ex6.dat', 'title': None, 'width': 50,
                    'format': '{:<5.2f}', 'suffix': '', 'no_labels': False,
                    'color': '{red, blue}', 'vertical': False, 'stacked': True,
                    'different_scale': False, 'calendar': False,
                    'start_dt': None, 'custom_tick': '', 'delim': '',
                    'verbose': False, 'version': False}

            # Value for color:
            # 'red': 91,
            # 'blue': 94,
            # 'green': 92,
            # 'magenta': 95,
            # 'yellow': 93,
            # 'black': 90,
            # 'cyan': 96
            colors = [91, 94]

            termgr.stacked_graph(labels, data, normal_data, args, colors)
            output = output.getvalue().strip()
            assert output == '2007: [91m▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇[0m[94m▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇[0m 373.84\n' \
                             '2008: [91m▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇[0m[94m▏[0m 236.23\n' \
                             '2009: [91m▇[0m[94m▇▇▇▇▇[0m 69.53\n2010: [91m▇▇▇▇[0m[94m▏[0m 57.21\n' \
                             '2011: [91m▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇[0m 508.97\n' \
                             '2012: [91m▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇[0m[94m▇[0m 232.25\n2014: [91m▏[0m[94m▏[0m 19.00'

    def test_vertically(self):
        args = {'filename': 'data/ex6.dat', 'title': None, 'width': 50,
                'format': '{:<5.2f}', 'suffix': '', 'no_labels': False,
                'color': None, 'vertical': True, 'stacked': False,
                'different_scale': False, 'calendar': False, 'start_dt': None,
                'custom_tick': '', 'delim': '', 'verbose': False,
                'version': False}
        # for value use ex6.dat category 1 first value
        value = 183.32
        num_blocks = 4
        result = termgr.vertically(value, num_blocks, args)
        assert result == [('▇',), ('▇',), ('▇',), ('▇',)]

    def test_chart(self):
        with patch('sys.stdout', new=StringIO()) as output:
            colors = [91, 94]

            # Use data from ex6.dat
            data = [[183.32, 190.52],
                    [231.23, 5.0],
                    [16.43, 53.1],
                    [50.21, 7],
                    [508.97, 7],
                    [212.05, 20.2],
                    [10.0, 9]]

            # positional arguments:
            #   filename              data file name (comma or space separated). Defaults to
            #                         stdin.
            #
            # optional arguments:
            #   -h, --help            show this help message and exit
            #   --title TITLE         Title of graph
            #   --width WIDTH         width of graph in characters default:50
            #   --format FORMAT       format specifier to use.
            #   --suffix SUFFIX       string to add as a suffix to all data points.
            #   --no-labels           Do not print the label column
            #   --color [{red,blue,green,magenta,yellow,black,cyan} [{red,blue,green,magenta,yellow,black,cyan} ...]]
            #                         Graph bar color( s )
            #   --vertical            Vertical graph
            #   --stacked             Stacked bar graph
            #   --different-scale     Categories have different scales.
            #   --calendar            Calendar Heatmap chart
            #   --start-dt START_DT   Start date for Calendar chart
            #   --custom-tick CUSTOM_TICK
            #                         Custom tick mark, emoji approved
            #   --delim DELIM         Custom delimiter, default , or space
            #   --verbose             Verbose output, helpful for debugging
            args = {'filename': 'data/ex6.dat', 'title': None, 'width': 50,
                    'format': '{:<5.2f}', 'suffix': '', 'no_labels': False,
                    'color': None, 'vertical': False, 'stacked': False,
                    'different_scale': False, 'calendar': False,
                    'start_dt': None, 'custom_tick': '', 'delim': '',
                    'verbose': False, 'version': False}

            # Use data from ex6.dat
            labels = ['2007', '2008', '2009', '2010', '2011', '2012', '2014']

            termgr.chart(colors, data, args, labels)
            output = output.getvalue().strip()
            assert output == '2007: [91m▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇[0m 183.32\n      [94m▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇[0m 190.52\n2008: [91m▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇[0m 231.23\n      [94m▇[0m 5.00 \n2009: [91m▇▇▇▇[0m 16.43\n      [94m▇▇▇▇▇▇▇▇▇▇▇▇▇[0m 53.10\n2010: [91m▇▇▇▇▇▇▇▇▇▇▇▇▇[0m 50.21\n      [94m▇[0m 7.00 \n2011: [91m▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇[0m 508.97\n      [94m▇[0m 7.00 \n2012: [91m▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇[0m 212.05\n      [94m▇▇▇▇▇[0m 20.20\n2014: [91m▇▇[0m 10.00\n      [94m▇▇[0m 9.00'